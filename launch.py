import sys
import os
import os.path
import subprocess
import json

# Gather paths
scripts_path   = os.path.dirname(__file__)
simulator_path = "%s/launcher" % (scripts_path)
binary         = "%s/launcher/manifold_launcher" % (scripts_path)
job_path       = os.getcwd()
outFileName    = "%s/output.raw" % job_path
errFileName    = "%s/error.raw" % job_path
benchPath      = os.path.realpath("/simulators/benchmarks/qsim/")
manifoldPath   = os.path.realpath("%s/../manifold-0.11.1/simulator/" % (scripts_path))
qsimStatePath  = os.path.realpath("%s/../qsim-0.1.5/" % (scripts_path))

# set environment variables needed to run
os.environ['LD_LIBRARY_PATH'] = '%s/lib' % (qsimStatePath)
os.environ['QSIM_PREFIX']     = qsimStatePath

# Generate input
outFile = open(outFileName, "w")
errFile = open(errFileName, "w")
print("launch.py: Generating simulator input files", file=outFile)
result  = subprocess.call(["python", "%s/config.py" % (scripts_path), scripts_path, job_path], stdout=outFile, stderr=errFile)
if (result != 0):
  print("launch.py: Failed to generate configuration", file=errFile)
  outFile.close()
  errFile.close()
  sys.exit(1)

# reopen files in append mode
print("", file=outFile)
print("", file=errFile)

print("launch.py: Loading simulator configuration", file=outFile)
fp = open("%s/input.json" % job_path)
conf = json.load(fp)
fp.close()

job_name = os.path.basename(job_path)

# sanity check for user paths
benchmark = benchPath     + conf['qsim']['benchmark']
statefile = qsimStatePath + conf['qsim']['statefile']
manifold  = manifoldPath  + conf['qsim']['manifold']

print("launch.py: Using benchmark %s" % (benchmark), file=outFile)

shouldClose = 0
if (os.path.commonprefix([os.path.normpath(benchmark), benchPath]) != benchPath):
  print("Sanity check failed for benchmark: %s" % conf['qsim']['benchmark'], file=errFile)
  shouldClose = 1
elif (os.path.commonprefix([os.path.normpath(statefile), qsimStatePath]) != qsimStatePath):
  print("Sanity check failed for state file: %s" % conf['qsim']['statefile'], file=errFile)
  shouldClose = 1
elif (os.path.commonprefix([os.path.normpath(manifold), manifoldPath]) != manifoldPath):
  print("Sanity check failed for manifold binary: %s" % conf['qsim']['manifold'], file=errFile)
  shouldClose = 1
if (shouldClose):
  outFile.close()
  errFile.close()
  exit(1)

# Form arguments
args = [
  binary,
  "%s/remote/server/qsim-server" % (qsimStatePath),
  benchmark,
  statefile,
  str(conf['qsim']['port']),
  "/usr/bin/mpirun",
  manifold,
  "%s/zesto.cfg" % job_path,
  "%s/manifold.cfg" % job_path,
  job_name
]

# Run
print("launch.py:running %s" % (repr(args)), file=outFile)
result = subprocess.call(args, stdout=outFile, stderr=errFile)
if (result != 0):
  print("launch.py: Failed to run simulator", file=errFile)
  outFile.close()
  errFile.close()
  sys.exit(1)

# Gather results
print("launch.py:parsing results")
result = subprocess.call(["python", "%s/parse.py" % (scripts_path)], stdout=outFile, stderr=errFile)
if (result != 0):
  print("launch.py: Failed to parse output", file=errFile)
  outFile.close()
  errFile.close()
  sys.exit(1)

outFile.close()
errFile.close()
sys.exit(0)
