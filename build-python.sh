# python requires ssl libraries to install :(
echo 'y' | apt-get install libssl-dev

# Install python
if [ ! -d pyenv ]
then
  git clone git://github.com/yyuu/pyenv.git pyenv

  export PYENV_ROOT=`pwd`/pyenv
  export PATH="$PYENV_ROOT/bin:$PATH"
  eval "$(pyenv init -)"

  pyenv install 2.7.6
  pyenv install 3.4.0
fi
