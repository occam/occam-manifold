cd .. # root

# Place install path in our 'usr' path
export PREFIX=`pwd`/usr

# Place usr/bin in path
export PATH=`pwd`/usr/bin:$PATH

# We will build for a 32-bit gnu/linux
export TARGET=i686-pc-linux-gnu

# Make our usr path
mkdir -p usr

# Build 32-bit binutils
wget http://ftp.gnu.org/gnu/binutils/binutils-2.22.tar.gz
tar -xvf binutils-2.22.tar.gz

mkdir -p binutils-obj
cd binutils-obj
../binutils-2.22/configure --target=i686-pc-linux-gnu --disable-nls --prefix=`pwd`/../usr
make
make install
cd .. # root

# Build gcc deps
wget http://ftp.gnu.org/pub/gnu/gmp/gmp-4.1.4.tar.gz
wget http://ftp.gnu.org/pub/gnu/mpfr/mpfr-2.4.2.tar.gz 

tar -xvf gmp-4.1.4.tar.gz
tar -xvf mpfr-2.4.2.tar.gz

# Build GMP
mkdir -p gmp-obj
cd gmp-obj
../gmp-4.1.4/configure --prefix=`pwd`/../usr
make
make install
cd .. # root

# Build MPFR
mkdir -p mpfr-obj
cd mpfr-obj
../mpfr-2.4.2/configure --prefix=`pwd`/../usr
make
make install
cd .. # root

# Build gcc
wget http://ftp.gnu.org/gnu/gcc/gcc-4.4.3/gcc-core-4.4.3.tar.gz
wget http://ftp.gnu.org/gnu/gcc/gcc-4.4.3/gcc-g++-4.4.3.tar.gz

tar -xvf gcc-core-4.4.3.tar.gz
tar -xvf gcc-g++-4.4.3.tar.gz

mkdir -p gcc-obj
cd gcc-obj
../gcc-4.4.3/configure --prefix=`pwd`/../usr --build=x86_64-pc-linux-gnu -host=i686-pc-linux-gnu --target=i686-pc-linux-gnu --without-headers --disable-nls --with-gmp=`pwd`/../usr --with-mpfr=`pwd`/../usr --enable-languages=c,c++
make all-gcc
make all-target-libgcc
make install-target-libgcc
cd .. # root

cd occam # return to occam script path
