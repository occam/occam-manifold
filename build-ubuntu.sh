echo 'y' | apt-get install zlib1g-dev git bison flex dc gcc-4.4 g++-4.4 wget make libfuse2 libconfig-dev libconfig++-dev

cd .. # root
mkdir -p fuse
cd fuse

apt-get download fuse
dpkg-deb -x fuse_* .
dpkg-deb -e fuse_*
rm fuse_*.deb
echo -en '#!/bin/bash\nexit 0\n' > DEBIAN/postinst
dpkg-deb -b . /fuse.deb
dpkg -i /fuse.deb

cd .. # root
cd occam # return to occam scripts

echo 'y' | apt-get install mpic++
