#include "manifold_validate.h"

#include <libconfig.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <stdint.h>

static int isPowerOf2(unsigned d)
{
  unsigned i;

  if (d==0)
    return 1;

  for (i=1; i!=0; i=i<<1)
  {
    if (d == i)
      return 1;
  }

  return 0;
}

static void dump_libconfig_error(config_t *conf)
{
   fprintf(stderr, "manifold_validate_cpu_count:libconfig:FILE %s:%s:line %d\n", config_error_file(conf), config_error_text(conf), config_error_line(conf));
}

// returns number of CPUs on success, -1 on error,
// 0 if mismatch
// number of CPUs should be power of 2
int manifold_validate_cpu_count(const char *stateFile, const char *manifoldConfigFile, long long* cycle_count)
{
  // find number of cores used by stateFile
  size_t stateLen = strlen(stateFile);
  int i;
  int dotPosition; // ends with .32, for instance

  for (i=stateLen-1; i>0; i--)
  {
    if (stateFile[i] == '.')
    {
      dotPosition = i;
      break;
    }
    else if (!isdigit(stateFile[i]))
    {
      fprintf(stderr, "manifold_validate_cpu_count:Statefile does not end in .integer\n");
      return -1;
    }
  }
  if (dotPosition == stateLen - 1)
  {
    fprintf(stderr, "manifold_validate_cpu_count:Statefile does not end in .integer\n");
    return -1;
  }

  int stateFileCores = atoi(stateFile+dotPosition+1);
  if (stateFileCores == 0 || !isPowerOf2(stateFileCores))
  {
    fprintf(stderr, "manifold_validate_cpu_count:Number of cores must be power of 2\n");
    return -1;
  }

  // initialize libconfig
  FILE *fp = fopen(manifoldConfigFile, "r");
  if (!fp)
  {
    perror("manifold_validate_cpu_count:fopen");
    return -1;
  }

  int result = -1;
  config_t conf;

  config_init(&conf);

  if (config_read(&conf, fp) != CONFIG_TRUE)
  {
    dump_libconfig_error(&conf);
    goto cleanup;
  }

  // validate frequency
  if (config_lookup_int64(&conf, "simulation_stop", cycle_count) != CONFIG_TRUE)
  {
    dump_libconfig_error(&conf);
    goto cleanup;
  }

  if (cycle_count <= 0)
  {
    fprintf(stderr, "manifold_validate_cpu_count:invalid simulation_stop\n");
    goto cleanup;
  }

  config_setting_t *cst;
  if (!(cst=config_lookup(&conf, "processor.node_idx")))
  {
    fprintf(stderr, "manifold_validate_cpu_count: could not find processor.node_idx\n");
    goto cleanup;
  }

  if (config_setting_is_array(cst) == CONFIG_FALSE)
  {
    fprintf(stderr, "manifold_validate_cpu_count: processor.node_idx is not array\n");
    goto cleanup;
  }

  int arrayLen;
  arrayLen = config_setting_length(cst);
  if (arrayLen != stateFileCores)
    result = 0;
  else
    result = arrayLen;

// CLEANUP
  cleanup:
  config_destroy(&conf);
  if (fp) fclose(fp);

  return result;
}

