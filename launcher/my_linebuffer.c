#include "my_linebuffer.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int my_linebuffer_init(my_linebuffer_t* lb)
{
  lb->data = malloc(1024);
  if (!lb->data)
    return -1;

  lb->data[0]	= 0;
  lb->start	= 0;
  lb->end	= 0;
  lb->size	= 1024;

  return 0;
}

void my_linebuffer_clear(my_linebuffer_t* lb)
{
  free(lb->data);
  lb->data = 0;
}

int my_linebuffer_add(my_linebuffer_t* lb, const char* s)
{
  if (lb->start != 0) // move all strings to the front
  {
    memmove(lb->data, lb->data+lb->start, lb->end - lb->start + 1);
    lb->end = lb->end - lb->start;
    lb->start = 0;
  }

  if (lb->start == lb->end)
  {
    lb->start = lb->end = 0;
    lb->data[0] = '\0';
  }

  // resize buffer if needed
  char* newBuf;
  int   sLen;
  int   sizeNeeded;

  sLen		= strlen(s);
  sizeNeeded	= lb->end + sLen + 1; 
  if (sizeNeeded >= lb->size)
  {
    newBuf = realloc(lb->data, sizeNeeded*2);
    if (!newBuf) // damn, out of memory
      return -1;
    lb->data  = newBuf;
    lb->size = sizeNeeded*2;
  }

  // copy in new string
  memmove(lb->data + lb->end, s, sLen+1);
  lb->end = lb->end+sLen;
  return 0;
}

const char* my_linebuffer_get(my_linebuffer_t* lb)
{
  char *result = 0;

  if (lb->start == lb->end)
    return 0;

  int newLinePos;
  for (newLinePos=lb->start; newLinePos != lb->end; newLinePos++)
  {
    if (lb->data[newLinePos] == '\n')
    {
      // printf("yay: %d\n", lb->data[lb->start]);

      result = lb->data + lb->start;
      lb->data[newLinePos] = '\0';
      lb->start = newLinePos + 1;

      break;
    }
  }

  return result;
}

const char* my_linebuffer_empty(my_linebuffer_t* lb)
{
  if (lb->start == lb->end)
    return 0;

  return lb->data + lb->start;
}

