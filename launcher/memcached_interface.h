#pragma once

#include <stddef.h>

int mc_init();
int mc_set(const char* key, size_t key_length, const char* data,
	size_t data_length);
void mc_free();

