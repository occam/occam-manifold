#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/select.h>

#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <errno.h>

#include "my_linebuffer.h"
#include "manifold_validate.h"

// parameter defines
#define QSIM_BINARY			1
#define QSIM_BENCHMARK			2
#define QSIM_STATE_FILE			3
#define QSIM_PORT			4
#define MPI_BINARY			5
#define MANIFOLD_BINARY			6
#define MANIFOLD_CONFIG_FILE		7
#define MANIFOLD_PROCESSOR_MODEL	8
#define MANIFOLD_JOB_NAME		9
#define PARAMETER_COUNT                 10

// process defines
#define MAIN_PROC			0	
#define QSIM_PROC			1
#define MANIFOLD_PROC			2
#define MEMCACHED_PROC			3
#define NUM_PROCS			4

const char * const PIPE_OWNER_NAMES[] = {
  "main",
  "qsim",
  "manifold",
  "memcached"
};

// pipe fd indices
#define QSIM_STDOUT_READ_PIPE		0
#define QSIM_STDOUT_WRITE_PIPE		1
#define QSIM_STDERR_READ_PIPE		2
#define QSIM_STDERR_WRITE_PIPE		3

#define MANIFOLD_STDOUT_READ_PIPE	4
#define MANIFOLD_STDOUT_WRITE_PIPE	5
#define MANIFOLD_STDERR_READ_PIPE	6
#define MANIFOLD_STDERR_WRITE_PIPE	7

#define MEMCACHED_STDOUT_READ_PIPE	8
#define MEMCACHED_STDOUT_WRITE_PIPE	9
#define MEMCACHED_STDERR_READ_PIPE	10
#define MEMCACHED_STDERR_WRITE_PIPE	11

// main thread writes to the next two pipes. the write pipe must be non-blocking
// to avoid deadlock
#define MEMCACHED_READ_PIPE		12
#define MEMCACHED_WRITE_PIPE		13

#define SIGNAL_READ_PIPE		14
#define SIGNAL_WRITE_PIPE		15

#define NUM_PIPES			8
#define NUM_PIPE_FDS			(NUM_PIPES*2)
#define NUM_MAIN_READ_PIPES		7

static int pipes[NUM_PIPE_FDS];
static int main_pipes[NUM_PIPES]; // pipes accessed by main

static const int PIPE_NONBLOCK[] = {
  MEMCACHED_WRITE_PIPE,
  SIGNAL_READ_PIPE,
  SIGNAL_WRITE_PIPE
};

#define NUM_PIPES_NONBLOCK 3

// who should close the pipes
static const int PIPE_FD_OWNERS[] = {
  MAIN_PROC,		// QSIM
  QSIM_PROC,
  MAIN_PROC,
  QSIM_PROC,
  MAIN_PROC,		// MANIFOLD
  MANIFOLD_PROC,
  MAIN_PROC,
  MANIFOLD_PROC,
  MAIN_PROC,		// MEMCACHED
  MEMCACHED_PROC,
  MAIN_PROC,
  MEMCACHED_PROC,
  MEMCACHED_PROC,	// For sending data to MEMCACHED
  MAIN_PROC,
  MAIN_PROC,		// signal pipes
  MAIN_PROC
};

// who is on the other end of the pipe for main
static const int MAIN_PIPE_SOURCE[] = {
  QSIM_PROC,
  QSIM_PROC,
  MANIFOLD_PROC,
  MANIFOLD_PROC,
  MEMCACHED_PROC,
  MEMCACHED_PROC,
  MEMCACHED_PROC, // outgoing pipe
  MAIN_PROC // signal pipe
};

static const char* const MAIN_PIPE_NAMES[] = {
  "qsim:stdout",
  "qsim:stderr",
  "manifold:stdout",
  "manifold:stderr",
  "memcached:stdout",
  "memcached:stderr",
  "memcached:write",
  "signal:read"
};

const int MAIN_PIPE_IS_READ[] = { // so main pipes are proper ends
  1,
  1,
  1,
  1,
  1,
  1,
  0,
  1
};

static volatile sig_atomic_t gShouldQuit = 0;

int interpret_manifold_progress(
  const char* input,
  long long* cpuCycles,
  int numCpus,
  long long numCycles
);


// closed pipes will be set to -1
int close_other_pipes(int me, int *pipeArray, const int *pipeOwners, int numPipes)
{
  int i;

  for (i=0; i<2*numPipes; i++)
  {
    if (pipeOwners[i] != me)
    {
      if (pipeArray[i] < 0) // pipe already closed
        continue;
      while (close(pipeArray[i]) == -1)
      {
        if (errno != EINTR)
        {
          perror("error:close_other_pipes()");
          return 0;
        }
      }
      pipeArray[i] = -1;
    }
  }

  return 1;
}

// closed pipes will be set to -1
int close_my_pipes(int me, int *pipeArray, const int *pipeOwners, int numPipes)
{
  int i;
  for (i=0; i<2*numPipes; i++)
  {
    if (pipeOwners[i] == me)
    {
      if (pipeArray[i] < 0) // pipe already closed
        continue;
      while (close(pipeArray[i]) == -1)
      {
        if (errno != EINTR)
        {
          perror("error:close_my_pipes()");
          return 0;
        }
      }
      pipeArray[i] = -1;
    }
  }

  return 1;
}

int close_all_pipes(int *pipeArray, int numPipes)
{
  int i;

  for (i=0; i<2*numPipes; i++)
  {
    if (pipeArray[i] < 0) // pipe already closed
      continue;
    while (close(pipeArray[i]) == -1)
    {
      if (errno != EINTR)
      {
        perror("error:close_my_pipes()");
        return 0;
      }
    }
    pipeArray[i] = -1;
  }

  return 1;
}

int validate_parameters(int argc, char* const* argv, int *cpuCount, long long *numCycles)
{
  if (argc != PARAMETER_COUNT)
  {
    printf("Usage: manifold_launcher <qsim_binary> <qsim_benchmark> <qsim_state_file> <qsim_port> <mpi_binary> <manifold_binary> <manifold_config_file> <manifold_processor_file> <job_name>\n");
    return 0;
  }
  
  if ((*cpuCount=manifold_validate_cpu_count(argv[QSIM_STATE_FILE], argv[MANIFOLD_CONFIG_FILE], numCycles)) <= 0)
  {
    return 0;
  }

  return 1;
}

// set pipe array values to -1 for easy cleanup
static void initialize_pipe_array(int *pipeArray, int *pipeReadArray, int numPipes)
{
  int i;
  for (i=0; i<numPipes; i++)
  {
    pipeArray[2*i]   = -1;
    pipeArray[2*i+1] = -1;
    pipeReadArray[i] = -1;
  }
}

// does not try to close negative fds in case they were already closed. Sets
// elements of array to -1 after close. Does not close read pipe array, but
// reinitializes its values to -1. If a desciptor fails to close, its value
// is not changed.
static void close_pipes(int *pipeArray, int *mainPipeArray, int numPipes)
{
  int i;
  for (i=0;i<numPipes*2; i++)
  {
    while (pipeArray[i] >= 0)
    {
      if (close(pipeArray[i]))
      {
        if (errno != EINTR)
        {
          perror("warn:close_pipes()");
          break;
        }
      }
      else
      {
        pipeArray[i] = -1;
      }
    }
  }

  for (i=0; i<numPipes*2; i++)
  {
    mainPipeArray[i] = -1;
  }
}

// all values in pipe arrays to -1 on failure
static int create_pipes(int *pipeArray, int *mainPipeArray, const int *mainPipeIsRead, int numPipes)
{
  // set array to negative values for cleanup
  int i;

  initialize_pipe_array(pipeArray, mainPipeArray, numPipes);

  for (i=0; i<numPipes; i++)
  {
    if (pipe(pipeArray+(2*i)))
    {
      perror("error:create_pipes()");
      close_pipes(pipeArray, mainPipeArray, numPipes);
      return 0;
    }
  }

  for (i=0; i<numPipes; i++)
  {
    if (mainPipeIsRead[i])
      mainPipeArray[i] = pipeArray[i*2];
    else
      mainPipeArray[i] = pipeArray[i*2+1];
  }

  for (i=0; i<2*numPipes; i++)
  {
    printf ("Pipe fd %d:%d\n", i, pipeArray[i]);
  }

  for (i=0; i<numPipes; i++)
  {
    printf ("Main pipe fd %d:%d\n", i, mainPipeArray[i]);
  }

  return 1;
}

static int set_pipes_nonblock(const int *pipeArray, const int *nonBlockArray, size_t nBlockArrLen)
{
  size_t	i;
  int		thePipe;
  int		flags;

  for (i=0; i<nBlockArrLen; i++)
  {
    thePipe = pipeArray[nonBlockArray[i]];
    do
    {
      flags = fcntl(thePipe, F_GETFL);
      if (flags == -1) // get current flags
      {
        if (errno != EINTR)
        { 
          perror("error:set_pipes_nonblock()");
          return 0;
        }
      }
    } while (flags == -1);

    flags |= O_NONBLOCK;

    while (fcntl(thePipe, F_SETFL, flags) == -1) // update flags
    {
      if (errno != EINTR)
      {
        perror("error:set_pipes_nonblock()");
        return 0;
      }
    }
  }

  return 1;
}

// handler for things that should cause termination
static void signal_handler(int sig)
{
  int oldErrno = errno;

  // write(STDOUT_FILENO, "SIGNAL HANDLER INVOKED\n",23);
  if (sig == SIGINT || sig == SIGTERM)
  {
    // write(STDOUT_FILENO, "SHOULD DIE\n", 11);
    gShouldQuit = 1;
  }

  while (write(pipes[SIGNAL_WRITE_PIPE], ".", 1) == -1) // self-pipe trick
  {
    if (errno == EINTR)
    {
      // write(STDOUT_FILENO, "A\n", 2);
      continue;
    }
    else if (errno == EWOULDBLOCK || errno == EAGAIN)
    {
      // write(STDOUT_FILENO, "B\n", 2);
      break;
    }
    else
    {
      // write(STDOUT_FILENO, "C\n", 2);
      abort();
    }
  }

  // write(STDOUT_FILENO, "RET\n", 4);
  errno = oldErrno;
}

// mask signals used by this program
static int mask_signals()
{
  sigset_t signalMask;

  if (sigfillset(&signalMask))
  {
    perror("error:sigemptyset()");
    return 0;
  }

  if (sigprocmask(SIG_BLOCK, &signalMask, 0))
  {
    perror("error:sigprocmask()");
    return 0;
  }

  return 1;
}

static int unmask_signals()
{
  sigset_t signalMask;

  if (sigfillset(&signalMask))
  {
    perror("error:sigemptyset()");
    return 0;
  }

  if (sigprocmask(SIG_UNBLOCK, &signalMask, 0))
  {
    perror("error:sigprocmask()");
    return 0;
  }

  return 1;
}

int install_signal_handlers()
{
  struct sigaction sa = {0};

  sa.sa_handler	= signal_handler;
  sa.sa_flags   = SA_RESTART;
  sigfillset(&(sa.sa_mask));

  // install handlers
  if (sigaction(SIGINT, &sa, 0))
  {
    perror("error:install_signal_handlers()");
    return 0;
  }
  if (sigaction(SIGTERM, &sa, 0))
  {
    perror("error:install_signal_handlers()");
    return 0;
  }
  sa.sa_flags |= SA_NOCLDSTOP;
  if (sigaction(SIGCHLD, &sa, 0))
  {
    perror("error:install_signal_handlers()");
    return 0;
  }

  return 1;
}

// signal mask is undefined on failure
int install_default_handlers()
{
  // set default handlers
  struct sigaction sa = {0};
  sa.sa_handler	= SIG_DFL;
  sa.sa_flags	= 0;

  if (sigemptyset(&(sa.sa_mask)))
  {
    perror("error:install_default_handlers()");
    return 0;
  }

  if (sigaction(SIGTERM, &sa, 0))
  {
    perror("error:install_default_handlers()");
    return 0;
  }
  if (sigaction(SIGINT, &sa, 0))
  {
    perror("error:install_default_handlers()");
    return 0;
  }
  if (sigaction(SIGCHLD, &sa, 0))
  {
    perror("error:install_default_handlers()");
    return 0;
  }

  return 1;
}


// does not redirect streams if they are -1
int redirect_output(int newStdout, int newStderr)
{
  if (newStdout >= 0) // redirect stdout
  {
    while (dup2(newStdout, STDOUT_FILENO) == -1)
    {
      if (errno != EINTR)
      {
        perror("dup2()");
        return 0;
      }
    }
  }

  if (newStderr >= 0) // redirect stderr
  {
    while (dup2(newStderr, STDERR_FILENO) == -1)
    {
      if (errno != EINTR)
      {
        perror("dup2()");
        return 0;
      }
    }
  }

  return 1;
}

void memcached_process(char **args)
{
  // remove any handlers installed by main process
  if (!install_default_handlers())
  {
    exit(EXIT_FAILURE);
  }
  // remove mask that prevented handlers from executing
  if (!unmask_signals())
  {
    exit(EXIT_FAILURE);
  }

  // redirect streams to pipes
  if (!redirect_output(
    pipes[MEMCACHED_STDOUT_WRITE_PIPE],
    pipes[MEMCACHED_STDERR_WRITE_PIPE]
  )) {
    exit(EXIT_FAILURE);
  }

  // close pipes I don't use
  if (!close_other_pipes(MEMCACHED_PROC, pipes, PIPE_FD_OWNERS, NUM_PIPES))
  {
    exit(EXIT_FAILURE);
  }


  if (mc_init())
  {
    exit(EXIT_FAILURE);
  }

  int           pipeFd = pipes[MEMCACHED_READ_PIPE];
  fd_set        pipeSet;
  int           shouldRun = 1;

  while (shouldRun)
  {
    FD_ZERO(&pipeSet);
    FD_SET(pipeFd, &pipeSet);
    if (select(pipeFd+1, &pipeSet, 0, 0, 0) == -1)
    {
      if (errno != EINTR)
      {
        perror("memcached:select()");
        shouldRun = 0;
      }
    }
    else
    {
      if (FD_ISSET(pipeFd, &pipeSet))
      {
        unsigned char buf[1024];
        size_t bytesRead;

        while ((bytesRead = read(pipeFd, buf, 1024)) == -1)
        {
          if (errno == EINTR)
            continue;
          else if (errno == EWOULDBLOCK || errno == EAGAIN)
          {
            break;
          }
          else
          {
            perror("memcached:read()");
            exit(EXIT_FAILURE);
          }
        }

        if (bytesRead == 0)
        {
          fprintf(stderr, "memcached:pipe closed\n");
          exit(0);
        }
        else if (bytesRead > 0)
        {
          unsigned char statusByte = buf[bytesRead-1];
          double value = (double)statusByte/255.0;

          sprintf(buf,
            "{"
              "\"completion\":%f"
            "}",
            value
          );

          // printf("%s\n", buf);

          if (mc_set(
            args[MANIFOLD_JOB_NAME],
            strlen(args[MANIFOLD_JOB_NAME]),
            buf,
            strlen(buf)
          ))
          {
            fprintf(stderr, "warn: failed to update memcached entry\n");
          }
        }
      }
    }
  }


  if (!close_my_pipes(MEMCACHED_PROC, pipes, PIPE_FD_OWNERS, NUM_PIPES))
  {
    exit(EXIT_FAILURE);
  }

  mc_free();

  printf("memcached process is terminating\n");

  exit(EXIT_FAILURE);
}

void qsim_process(char* const argv[])
{
  // remove any handlers installed by main process
  if (!install_default_handlers())
  {
    exit(EXIT_FAILURE);
  }
  // remove mask that prevented handlers from executing
  if (!unmask_signals())
  {
    exit(EXIT_FAILURE);
  }

  if (!redirect_output(
    pipes[QSIM_STDOUT_WRITE_PIPE],
    pipes[QSIM_STDERR_WRITE_PIPE]
  ))
  {
    exit(EXIT_FAILURE);
  }

  // close pipes I don't use
  if (!close_other_pipes(QSIM_PROC, pipes, PIPE_FD_OWNERS, NUM_PIPES))
  {
    exit(EXIT_FAILURE);
  }

  // launch QSIM
  char* const qsim_params[] =
  {
    argv[QSIM_BINARY],
    argv[QSIM_PORT],
    argv[QSIM_STATE_FILE], // TODO: ensure this matches MANIFOLD config
    argv[QSIM_BENCHMARK],
    0
  };

  execv(argv[QSIM_BINARY], qsim_params);

  perror("execv()");
  exit(EXIT_FAILURE);
}

// returns -1 on error, 0 if no children left, or 1 otherwise
// pid will be 0 if no process was reaped, pid if reaped
int reapProcess(pid_t *reapedPid, int *status)
{
  pid_t	waitResult;

  *reapedPid = 0;
  while ((waitResult=waitpid(-1, status, WNOHANG)) == -1)
  {
    if (errno == EINTR)
    {
      continue;
    }
    else if (errno == ECHILD)
    {
      return 0;
    }
    else
    {
      perror("error:waitpid()");
      return -1;
    }
  }
  
  if (waitResult > 0)
  {
    if (WIFEXITED(*status))
    {
      printf(
        "status:process %d has exited with code %d\n",
        waitResult,
        WEXITSTATUS(*status));
    }
    else
    {
      printf(
        "status:process %d terminated with signal %d\n",
        waitResult,
        WTERMSIG(*status));
    }
  }

  *reapedPid = waitResult;
 
  return 1;
}

int launch_manifold(pid_t* pid, char* const argv[], int numCores)
{
  *pid = -1; // pid may still be valid on failure

  if (!mask_signals)
    return -1;

  *pid = fork();
  if (*pid < 0)
  {
    perror("fork()");
    return -1;
  }
  else if (*pid == 0) // manifold subprocess
  {
    if (!install_default_handlers())
      exit(EXIT_FAILURE);

    if (!unmask_signals())
      exit(EXIT_FAILURE);

    if (!redirect_output(
      pipes[MANIFOLD_STDOUT_WRITE_PIPE],
      pipes[MANIFOLD_STDERR_WRITE_PIPE]))
    {
      exit(EXIT_FAILURE);
    }

    if (!close_other_pipes(MANIFOLD_PROC, pipes, PIPE_FD_OWNERS, NUM_PIPES))
      exit(EXIT_FAILURE);

    // convert number of cores+1 to string
    char coreString[64];
    sprintf(coreString, "%d", numCores+1);

    char *manifold_params[] = {
      argv[MPI_BINARY],
      "-np",
      coreString,
      argv[MANIFOLD_BINARY],
      argv[MANIFOLD_CONFIG_FILE],
      argv[MANIFOLD_PROCESSOR_MODEL],
      "127.0.0.1",
      argv[QSIM_PORT],
      0
    };

    execv(argv[MPI_BINARY], manifold_params);

    perror("error:execv()");
    exit(EXIT_FAILURE);
  }

  if (!unmask_signals())
  {
    return -1;
  }

  if (!close_my_pipes(MANIFOLD_PROC, pipes, PIPE_FD_OWNERS, NUM_PIPES))
    return -1;

  return 0;
}

int mainLoop(
  int* mainPipes,
  const int* mainPipeIsRead,
  int numMainPipes,
  int* pipes,
  pid_t* mcPid,
  pid_t* qsimPid,
  pid_t* manifoldPid,
  my_linebuffer_t* buffers,
  char* const argv[],
  int numCores,
  long long* cpuCycles,
  long long numCycles
)
{
  int selectResult = 0;
  int maxFd = -1;
  int i = 0;
  fd_set readFds;
  int processesOpen = 2;

  while (1) // main select loop
  {
    maxFd = -1;
    int manifoldLaunched = 0;

    // arm FDs
    FD_ZERO(&readFds);
    for (i=0;i<numMainPipes;i++)
    {
      if (mainPipes[i] >= 0 && mainPipeIsRead[i])
      {
        FD_SET(mainPipes[i], &readFds);
        if (mainPipes[i] > maxFd)
          maxFd = mainPipes[i];
      }
    }
    if (maxFd < 0)
    {
      fprintf(stderr, "error:all file desciptors closed\n");
      return -1;
    }

    selectResult = select(maxFd+1, &readFds, 0, 0, 0);
    if (selectResult == -1)
    {
      if (errno == EINTR)
        continue;
      else
      {
        perror("error:select()");
        return -1;
      }
    }
    for (i=0; i<numMainPipes; i++) // handle file desciptors
    {
      if (
        mainPipes[i] >= 0 &&
        mainPipeIsRead[i] &&
        FD_ISSET(mainPipes[i], &readFds)
      )
      {
        ssize_t bytesRead=0;
        char readBuf[256];

        if (mainPipes[i] == pipes[SIGNAL_READ_PIPE])
        {
          printf("debug:received signal in main loop\n");
          
          while (bytesRead=read(mainPipes[i], readBuf, sizeof(readBuf)) < 0)
          {
            if (errno == EINTR)
              continue;
            else if (errno == EWOULDBLOCK && errno == EAGAIN)
            {
              break;
            }
            else
            {
              perror("error:read()");
              return -1;
            }
          }
          if (gShouldQuit)
          {
           fprintf(stderr, "error:terminating due to signal\n");
           return -1;
          }

          
          pid_t reapedPid;
          int   reapedPidStatus;
          int   reapResult;

#if 1
          do
          {
            reapResult = reapProcess(&reapedPid, &reapedPidStatus);
            if (reapResult < 0)
            {
              return -1;
            }
            else if (reapResult == 0) // this shouldn't happen
            {
              return 0;
            }
            else if (reapedPid > 0)
            {
              processesOpen--;

              const char* processName = "unknown";
              if (reapedPid == *mcPid)
              {
                processName = "memcached";
                *mcPid = -1;
              }
              else if (reapedPid == *qsimPid)
              {
                processName = "qsim";
                *qsimPid = -1;
              }
              else if (reapedPid == *manifoldPid)
              {
                processName = "manifold";
                *manifoldPid = -1;
              }

              // memcached process dieing is not fatal
              if (reapedPidStatus && strcmp(processName, "memcached"))
              {
                fprintf(
                  stderr,
                  "error:[%s] process terminated abnormally\n",
                  processName
                );
                return -1;
              }
              else
              {
                fprintf(
                  stdout,
                  "info:[%s] process terminated normally\n",
                  processName
                );
              }
            }
            if (*qsimPid < 0 && *manifoldPid < 0)
              return 0;
          } while (reapedPid > 0);
#endif
        }
        else // incoming simulator output
        {
          while ((bytesRead=read(mainPipes[i], readBuf, sizeof(readBuf)-1)) == -1)
          {
            if (errno != EINTR)
            {
              perror("error:read()");
              return -1;
            }
          }
          // printf("bytesRead=%d\n", bytesRead);
          if (bytesRead == 0)
          {
            printf(
              "info:pipe %d from process %s has closed\n",
              mainPipes[i],
              PIPE_OWNER_NAMES[MAIN_PIPE_SOURCE[i]]
            );
            mainPipes[i] = -1;
          }
          else
          {
            // TODO: this code may have issue with newlines
            readBuf[bytesRead] = '\0';

            char *buf = strtok(readBuf,"\n");
            while (buf)
            {
              printf("%s:%s\n", MAIN_PIPE_NAMES[i], buf);
              if (mainPipes[i] == pipes[QSIM_STDOUT_READ_PIPE])
              {
                if (*manifoldPid < 0 && !strcmp("QSim server ready.", buf))
                {
                  printf("info:QSim ready\n");
                  printf("status:launching manifold\n");

                  if (launch_manifold(manifoldPid, argv, numCores))
                  {
                    fprintf(stderr, "error: could not launch manifold\n");
                    return EXIT_FAILURE;
                  }
                }
              }
              else if (mainPipes[i] == pipes[MANIFOLD_STDOUT_READ_PIPE]) // update our progress
              {
                // fprintf(stderr, "debug:got output from manifold\n");
                int		iResult;
                unsigned char	cResult;

                // printf("HMM\n");

                if ((iResult=interpret_manifold_progress(buf, cpuCycles, numCores, numCycles)) >= 0)
                {
                  cResult = (unsigned char)iResult;
                  write(pipes[MEMCACHED_WRITE_PIPE], &cResult, 1);
                } 
              }
              buf = strtok(0, "\n");
            }
          }
        }
      }
    }
  }

  return 0;
}

// we don't check for failure in this case
int kill_child_processes(pid_t mc, pid_t qsim, pid_t manifold)
{
  pid_t	pid;
  int	status;
  int   sentTerms = 0;
  int	sentKills = 0;

  mask_signals(); // we don't want interrupted

  while (1)
  {
    pid=waitpid(-1, &status, WNOHANG);
    if (pid == -1)
    {
      if (errno == ECHILD)
      {
        fprintf(stderr, "debug:ECHILD:all children terminated before cleanup\n");
        return;
      }
      else
      {
        perror("warn:waitpid() produced an error\n");
        break;
      }
    }
    else if (pid > 0) {
      const char *processName = "unknown";
      if (pid == mc)
      {
        processName = "memcached";
        mc = -1;
      }
      else if (pid == qsim)
      {
        processName = "qsim";
        qsim = -1;
      }
      else if (pid == manifold)
      {
        processName = "manifold";
        manifold = -1;
      }
      if (WIFEXITED(pid))
      {
        printf(
          "info:%s terminated with code %d\n",
          processName, 
          WEXITSTATUS(status));
      }
      else if (WIFSIGNALED(status))
      {
        printf(
          "info:%s terminated by signal %d\n",
          processName,
          WTERMSIG(status));
      }
      else
      {
        printf("info:%s terminated with raw code %d\n", processName, status);
      }

      continue;
    }

    printf("status:asking children to terminate nicely\n");

    if (!sentTerms) // ask children politely to terminate
    {
      printf("status:asking children to terminate nicely\n");
      sentTerms = 1;
      if (mc >= 0)
      {
        printf("status:asking memcached to terminate\n");
        kill(mc, SIGTERM);
      }
      if (qsim >= 0)
      {
        printf("status:asking qsim to terminate\n");
        kill(qsim, SIGTERM);
      }
      if (manifold >= 0)
      {
        printf("status:asking manifold to terminate\n");
        kill(manifold, SIGTERM);
      }

      printf("status:giving 10 seconds to terminate\n");
      sleep(10);
      continue;
    }
    else if (!sentKills)
    {
      sentKills = 1;
      printf("status:time for the -9 option\n");

      if (mc >= 0)
      {
        printf("status:forcing memcached to terminate\n");
        kill(mc, SIGKILL);
      }
      if (qsim >= 0)
      {
        printf("status:forcing qsim to terminate\n");
        kill(qsim, SIGKILL);
      }
      if (manifold >= 0)
      {
        printf("status:forcing manifold to terminate\n");
        kill(manifold, SIGKILL);
      }

      printf("status:giving 2 seconds to kill\n");
      sleep(2);
      continue;
    }
    else
    {
      printf("warning:could not close all children\n");
      return;
    }
  }
}

// returns -1 if error/failed to parse, 0-255 on success
int interpret_manifold_progress(
  const char* input,
  long long* cpuCycles,
  int numCpus,
  long long numCycles
)
{
  int i;
  int coreNum = -1;
  long long cycleNum = -1;

  const char* first;

  if (input[0] != 'T' && input[1] != '=')
    return -1;

  for (i=2; input[i]; i++)
  {
    if (isdigit(input[i]))
    {
      if (sscanf(input+i, "%lld %d", &cycleNum, &coreNum) != 2)
      {
        fprintf(stderr, "debug: got T= but could not parse progress\n");
        return -1;
      }

      if (
        coreNum < 0 ||
        coreNum >= numCpus ||
        cycleNum < 0 ||
        cycleNum > numCycles)
      {
        printf("stderr, debug: got T= but values parsed were not valid\n");
        return -1;
      }

      printf("coreNum=%d cycleNum=%lld\n", coreNum, cycleNum);
      cpuCycles[coreNum] = cycleNum;

      // recalculate progress
      long long total=0;
      int j;
      double d;

      for (j=0; j<numCpus; j++)
      {
        printf("debug:%lld\n", cpuCycles[j]);
        total += cpuCycles[j];
      }

      printf("debug:total=%lld numCycles=%lld\n", total, numCycles);

      if (total >= numCycles*numCpus) // some paranoia
        return 255;

      d = (double)total/((double)(numCpus*numCycles))*255.0;
      if (d > 255)
        return 255;
      if (d <= 0)
        return 0;
      j = (int)d;
      if (j < 0) return 0;
      if (j > 255) return 255;

      return j;
    }
    else if (!isspace(input[i]))
    {
      break;
    }
  }

  return -1;
}

int main(int argc, char* argv[])
{
  int cpuCount;
  long long numCycles;
  long long *cpuCycles=0;
  pid_t mcPid       = -1;
  pid_t qsimPid     = -1;
  pid_t manifoldPid = -1;

  // set arrays to known state
  printf("status:validating parameters\n");
  if (!validate_parameters(argc, argv, &cpuCount, &numCycles))
    return EXIT_FAILURE;
  printf("info:cpu_validation_stats=(%d,%lld)\n", cpuCount, numCycles);

  cpuCycles = malloc(cpuCount*(sizeof(long long)));
  if (!cpuCycles)
  {
    perror("error:malloc()");
    return EXIT_FAILURE;
  }
  memset(cpuCycles, 0, cpuCount*sizeof(long long));

  // create pipes
  printf("status:creating pipes\n");
  int result = 0;
  if (!create_pipes(pipes, main_pipes, MAIN_PIPE_IS_READ, NUM_PIPES))
  {
    result = EXIT_FAILURE;
    goto cleanup;
  }

  // set pipes as non-blocking
  printf("status:setting some pipes as non-blocking\n");
  if (!set_pipes_nonblock(pipes, PIPE_NONBLOCK, sizeof(PIPE_NONBLOCK)/sizeof(NUM_PIPES_NONBLOCK)))
  {
    result = EXIT_FAILURE;
    goto cleanup;
  }

  // install signal handlers
  printf("status:installing signal handlers\n");
  if (!install_signal_handlers())
  {
    result = EXIT_FAILURE;
    goto cleanup;
  }

  // sleep(10);

  // launch memcached process
  printf("status:launching memcached process\n");
  if (!mask_signals()) // make sure our signal handlers aren't invoked
  {
    result = EXIT_FAILURE;
    goto cleanup;
  }
  mcPid = fork();
  if (mcPid < 0)
  {
    perror("error:fork()");
    result = EXIT_FAILURE;
    goto cleanup;
  }
  else if (mcPid == 0)
  {
    memcached_process(argv);
  }
  else
  {
    printf("status:memcached process launched. closing pipes\n");
    if (!close_my_pipes(MEMCACHED_PROC, pipes, PIPE_FD_OWNERS, NUM_PIPES))
    {
      result = EXIT_FAILURE;
      goto cleanup;
    }
  }

  // signal handlers still masked
  printf("status:launching QSIM\n");
  qsimPid = fork();
  if (qsimPid < 0)
  {
    perror("error:fork()");
    result = EXIT_FAILURE;
    goto cleanup;
  }
  else if (qsimPid == 0)
  {
    qsim_process(argv);
  }
  else
  {
    printf("status:qsim process launched. closing pipes\n");
    if (!close_my_pipes(QSIM_PROC, pipes, PIPE_FD_OWNERS, NUM_PIPES))
    {
      result = EXIT_FAILURE;
      goto cleanup;
    }
  }

  // create output buffers; this actually creates more than necessary
  printf("status:creating buffers\n");
  my_linebuffer_t buffers[NUM_PIPES];
  int i;
  for (i=0; i<NUM_PIPES; i++)
  {
    if (my_linebuffer_init(buffers + i))
    {
      fprintf(stderr, "error:failed to create linebuffer\n");
      result = EXIT_FAILURE;
      goto cleanup;
    }
  }

  printf("status:starting main loop\n");
  if (!unmask_signals())
  {
    result = EXIT_FAILURE;
    goto cleanup;
  }
  if (mainLoop(
    main_pipes,
    MAIN_PIPE_IS_READ,
    NUM_PIPES,
    pipes,
    &mcPid,
    &qsimPid,
    &manifoldPid,
    buffers,
    argv,
    cpuCount,
    cpuCycles,
    numCycles)
  )
  {
    result = EXIT_FAILURE;
    goto cleanup;
  }

  printf("status: normal termination\n");

cleanup:
  // TODO: some output could be lost since we don't drain the buffers
  printf("status:closing pipes\n");
  kill_child_processes(mcPid, qsimPid, manifoldPid);
  close_pipes(pipes, main_pipes, NUM_PIPES); 

  return result;
}


