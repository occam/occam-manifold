#pragma once

typedef struct my_linebuffer_s
{
  char	*data;
  int	start;
  int	end;
  int	size;
} my_linebuffer_t;

// allocate/deallocate internal memory; 0 on success
int my_linebuffer_init(my_linebuffer_t* lb);
void my_linebuffer_clear(my_linebuffer_t* lb);

// add/remove; 0 on success
int my_linebuffer_add(my_linebuffer_t* lb, const char* s);
const char* my_linebuffer_get(my_linebuffer_t* lb);

// return any buffer remnants; must have exhausted my_linebuffer_get
// for this to work
const char* my_linebuffer_empty(my_linebuffer_t* lb);

