#include "manifold_validate.h"

#include <stdio.h>


int manifold_validate_cpu_count(const char *stateFile, const char *manifoldConfigFile, long long* cycle_count);

int main(int argc, char** argv)
{
  long long cycle_count;
  int result;

  result =  manifold_validate_cpu_count("/simulators/configs/qsim/state.2", "/simulators/source/manifold-0.11.1/simulator/smp/config/conf2x2_torus_l1l2.cfg", &cycle_count);

  printf("%d returned; %lld cycles\n", result, cycle_count);

  result = manifold_validate_cpu_count("/simulators/configs/qsim/state.1", "/simulators/source/manifold-0.11.1/simulator/smp/config/conf2x2_torus_l1l2.cfg", &cycle_count);
  printf("%d returned; %lld cycles\n", result, cycle_count);

  result = manifold_validate_cpu_count("/simulators/configs/qsim/state", "/simulators/source/manifold-0.11.1/simulator/smp/config/conf2x2_torus_l1l2.cfg", &cycle_count);
  printf("%d returned; %lld cycles\n", result, cycle_count);

  result = manifold_validate_cpu_count("/simulators/configs/qsim/state.4", "/simulators/source/manifold-0.11.1/simulator/smp/config/conf3x2_torus_llp.cfg", &cycle_count);
  printf("%d returned; %lld cycles\n", result, cycle_count);

  result = manifold_validate_cpu_count("/simulators/configs/qsim/state.3", "/simulators/source/manifold-0.11.1/simulator/smp/config/conf3x2_torus_llp.cfg", &cycle_count);
  printf("%d returned; %lld cycles\n", result, cycle_count);

  return 0;
}
