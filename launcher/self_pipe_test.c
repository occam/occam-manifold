#include <stdio.h>
#include <stdlib.h>

#include <sys/types.h>

#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <errno.h>

int pipefd[2];

volatile sig_atomic_t gShouldQuit = 0;

static volatile int numSignals = 10;

void term_handler(int signalId)
{
  gShouldQuit = 1;

  while (write(pipefd[1], "T", 1) == -1)
  {
    if (errno == EINTR)
      continue;
    else if (errno == EAGAIN || errno == EWOULDBLOCK)
    {
      break;
    }
    else // uh oh
    {
      abort();
    }
  }

  __sync_fetch_and_sub(&numSignals, 1);
}

void child_handler(int signalId)
{
  while (write(pipefd[1], "C", 1) == -1)
  {
    if (errno == EINTR)
      continue;
    else if (errno == EAGAIN || errno == EWOULDBLOCK)
    {
      break;
    }
    else // uh oh
    {
      abort();
    }
  }

  __sync_fetch_and_sub(&numSignals, 1);
}

int mask_signals()
{
  sigset_t s;

  if (sigfillset(&s))
  {
    perror("sigfillset()");
    return -1;
  }

  if (sigprocmask(SIG_BLOCK, &s, 0))
  {
    perror("sigprocmask()");
    return -1;
  }

  return 0;
}

int unmask_signals()
{
  sigset_t s;

  if (sigfillset(&s))
  {
    perror("sigfillset()");
    return -1;
  }

  if (sigprocmask(SIG_UNBLOCK, &s, 0))
  {
    perror("sigprocmask()");
    return -1;
  }

  return 0;
}

int install_signal_handlers()
{
  struct sigaction sa = {0};

  sa.sa_handler = term_handler;
  sa.sa_flags   = SA_RESTART;
  if (sigfillset(&(sa.sa_mask)))
  {
    perror("sigfillset()");
    return -1;
  }

  if (sigaction(SIGTERM, &sa, 0))
  {
    perror("sigaction()");
    return -1;
  }
  if (sigaction(SIGINT, &sa, 0))
  {
    perror("sigaction()");
    return -1;
  }
  sa.sa_handler = child_handler;
  if (sigaction(SIGCHLD, &sa, 0))
  {
    perror("sigaction()");
    return -1;
  }

  return 0;
}

int main(int argc, char *argv[])
{

  if (pipe(pipefd))
  {
    perror("pipe()");
    return EXIT_FAILURE;
  }

  int flags;
  int i;

  for (i=1;i<2;i++)
  {
    flags = fcntl(pipefd[i], F_GETFL);
    if (flags == -1)
    {
      perror("fcntl(GETFL)");
      return EXIT_FAILURE;
    }

    flags |= O_NONBLOCK;

    if (fcntl(pipefd[i], F_SETFL, flags))
    {
      perror("fcntl(SETFL)");
      return EXIT_FAILURE;
    }
  }

  // install signal handlers
  if (install_signal_handlers())
    return EXIT_FAILURE;

  char buf[256];
  ssize_t bytesRead;
  while (numSignals >= 0)
  {
    while (bytesRead = read(pipefd[0], buf, 255) == -1)
    {
      if (errno == EAGAIN || errno == EWOULDBLOCK || errno == EINTR)
      {
        perror("LOL");
        continue;
      }
      else
      {
        perror("read()");
        return EXIT_FAILURE;
      }
    }
    buf[bytesRead] = '\0';
    // printf("%s", buf);
    printf("lol\n");
  }

  puts("\nEnd");
  return 0;
}

