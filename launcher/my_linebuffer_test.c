#include "my_linebuffer.h"

#include <stdio.h>

char str1[] = "hello, world!";
char str2[] = "\nhow are you\nI am fine\n";
char str3[] = "Want to grab a bite?";
char str4[] = "\nOK, sounds great\n";

int main(int argc, char** argv)
{
  my_linebuffer_t lb;
  const char* line;

  my_linebuffer_init(&lb);

  printf("%d\n", my_linebuffer_add(&lb, str1));
  while (line=my_linebuffer_get(&lb))
  {
    printf("%s<END>\n", line);
  }

  printf("%d\n", my_linebuffer_add(&lb, str2));
  while (line=my_linebuffer_get(&lb))
  {
    printf("%s<END>\n", line);
  }

  printf("%d\n", my_linebuffer_add(&lb, str3));
  while (line=my_linebuffer_get(&lb))
  {
    printf("%s<END>\n", line);
  }

  printf("%d\n", my_linebuffer_add(&lb, str4));
  while (line=my_linebuffer_get(&lb))
  {
    printf("%s<END>\n", line);
  }

  my_linebuffer_clear(&lb);

  return 0;
}

