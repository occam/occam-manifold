cd .. # root

# symlink gcc-4.4
mkdir -p usr
mkdir -p usr/bin
ln -s /usr/bin/gcc-4.4 ./usr/bin/gcc
ln -s /usr/bin/g++-4.4 ./usr/bin/g++

export PATH=`pwd`/usr/bin:$PATH

# Acquire qsim
wget http://manifold.gatech.edu/wp-content/uploads/2014/02/qsim-0.1.5.tar.gz
tar -xvf qsim-0.1.5.tar.gz

cd qsim-0.1.5

# Run qsim's scripts to get (and patch) qemu
./getqemu.sh
cd qemu-0.12.3
make
cd .. # pwd: qsim-0.1.5

# Run qsim's scripts to get (and patch) linux
cd linux
./getkernel.sh
cd linux-2.6.34
cd .. # pwd: qsim-0.1.5/linux
cd .. # pwd: qsim-0.1.5

# Build qsim base
QSIM_PREFIX=`pwd` make install

cd remote

# Build qsim client
cd client
QSIM_PREFIX=`pwd`/../.. make install
cd ..

# Build qsim server
cd server
QSIM_PREFIX=`pwd`/../.. make install
cd ..

cd .. # pwd: qsim-0.1.5
cd .. # pwd: root

cd occam # return to the occam script path
