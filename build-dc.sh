cd .. # root

# Make our libraries path
mkdir -p usr

# Get dc
wget ftp://ftp.gnu.org/gnu/bc/bc-1.06.tar.gz
tar -xvf bc-1.06.tar.gz
cd bc-1.06

# Configure dc
./configure --prefix=`pwd`/../usr

# Build dc
make
make install

cd .. # root

cd occam # return to occam script path
