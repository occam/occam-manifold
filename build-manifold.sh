cd .. # root

# symlink gcc-4.4
mkdir -p usr
mkdir -p usr/bin
ln -s /usr/bin/gcc-4.4 ./usr/bin/gcc
ln -s /usr/bin/g++-4.4 ./usr/bin/g++

export PATH=`pwd`/usr/bin:$PATH

# Acquire manifold
wget http://manifold.gatech.edu/wp-content/uploads/2013/12/manifold-0.11.1.tar.gz
tar -xvf manifold-0.11.1.tar.gz

# Build manifold
cd manifold-0.11.1
./configure QSIMINC=`pwd`/../qsim-0.1.5/include --includedir=`pwd`/../qsim-0.1.5/include
make

cd .. # root
cd occam # return to occam scripts
