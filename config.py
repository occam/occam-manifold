import json
import sys
import os

script_folder = sys.argv[1]
output_folder = sys.argv[2]

zesto_file = open("%s/zesto.cfg" % output_folder, 'w')
mfold_file = open("%s/manifold.cfg" % output_folder, 'w')
data = json.load(open("%s/input.json" % output_folder, 'r'))
schema = json.load(open("%s/input_schema.json" % script_folder, 'r'))

zesto_dict = data['zesto']
zesto_schema = schema
for key, value in zesto_dict.items():
  value_type = schema['zesto'][key]['type']
  if (value_type == "string"):
    print("%s %s" % (key,str(value)), file=zesto_file)
  else:
    print("unknown value type: %s" % value_type, file=sys.stderr)
    exit(-1)
zesto_file.close()

manifold_dict = data['manifold']
for (k, v) in manifold_dict.items():
  schema_node = schema['manifold'][k]
  if ('type' in schema_node and isinstance(schema_node['type'], str)):
    value_type = schema_node['type']
    if (value_type == 'string'):
      print("%s = \"%s\";" % (k,v), file=mfold_file)
    elif (value_type == 'long'):
      print("%s = %sL;" % (k,str(v)), file=mfold_file)
    elif (value_type == 'int' or value_type == 'hex'):
      print("%s = %s;" % (k,str(v)), file=mfold_file)
    else:
      # print("Schema node=%s" % schema_node)
      print("Error: unknown value_type: %s" % value_type, file=sys.stderr)
      exit(-1)
  else:
    print("%s:\n{" % k, file=mfold_file)
    for (k2,v2) in v.items():
      value_type = schema_node[k2]['type']
      if (value_type == 'string'):
        print("\t%s = \"%s\";" % (k2,v2), file=mfold_file)
      elif (value_type == 'long'):
        print("\t%s = %sL;" % (k2,str(v2)), file=mfold_file)
      elif (value_type == 'int' or value_type == 'hex'):
        print("\t%s = %s;" % (k2,str(v2)), file=mfold_file)
      elif (value_type == 'array'):
        arr = v2.split()
        print("\t%s = [%s];" % (k2, ", ".join(arr)), file=mfold_file)
      else:
        # print("Schema node=%s" % schema_node)
        print("Error: unknown value_type: %s" % value_type, file=sys.stderr)
        exit(-1)
    print("};", file=mfold_file)
      
    

