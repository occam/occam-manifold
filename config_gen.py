import json
import sys

fp  = open(sys.argv[1], 'r')
out = open(sys.argv[2], 'w')

j = json.load(fp)

for x in j.values():
  for (k, v) in x.items():
    if ('default' in x[k]):
      x[k] = x[k]['default']
    else:
      for (k2, v2) in v.items():
        v[k2] = v[k2]['default']

print(json.dumps(j, sort_keys=True, indent=4, separators=(',', ': ')), file=out)

out.close()
fp.close()
