export PYENV_ROOT=`pwd`/pyenv
export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"

pyenv local 3.4.0

python launch.py
